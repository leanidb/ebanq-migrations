You have a database, which use Drupal (ex. ebanq).

In '.env' file at the root of the project you must add settings for connection with database and json file settings:
 
 DB_CONNECTION=mysql
 DB_HOST=127.0.0.1
 DB_PORT=3306
 DB_DATABASE=ebanq
 DB_USERNAME=root
 DB_PASSWORD=

 NUMBER_OF_RECORDS=1000
 PATH_TO_JSON_FILES=storage/app/public/

Then you must open a console and go to the root of the project folder in console, then you must write a next command:
 
 php artisan migrate:all
 
You can find generated files in next folder:
 
 'root of the project folder'/storage/app/public/

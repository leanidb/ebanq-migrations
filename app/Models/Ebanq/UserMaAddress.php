<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserMaAddress extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_ma_address';
}

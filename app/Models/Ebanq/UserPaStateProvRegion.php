<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserPaStateProvRegion extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_pa_state_prov_region';
}

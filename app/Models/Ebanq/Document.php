<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_documents';
}

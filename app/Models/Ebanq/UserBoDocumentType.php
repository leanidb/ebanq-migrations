<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserBoDocumentType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field__user_bo_document_type';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserMaAsPhysical extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_ma_as_physical';
}

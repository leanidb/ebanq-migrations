<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserBoRelationship extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_bo_relationship';
}

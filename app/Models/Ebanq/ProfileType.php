<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class ProfileType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_profile_type';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    protected $connection = 'mysql';

    protected $table = 'security_questions';

    protected $primaryKey = 'sqid';
}

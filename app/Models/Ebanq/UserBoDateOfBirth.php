<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserBoDateOfBirth extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_bo_date_of_birth';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserCountryOfResidence extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_country_of_residence';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserClass extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_class';
}

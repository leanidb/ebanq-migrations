<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserMaAddress2ndLine extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_ma_address_2nd_line_';
}

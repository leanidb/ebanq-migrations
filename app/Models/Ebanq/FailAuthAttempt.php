<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class FailAuthAttempt extends Model
{
    protected $connection = 'mysql';

    protected $table = 'ebanq_fail_auth_attempts';

    protected $primaryKey = 'id';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserPaZipPostalCode extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_pa_zip_postal_code';
}

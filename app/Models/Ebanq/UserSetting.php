<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $connection = 'mysql';

    protected $table = 'ebanq_user_settings';

    protected $primaryKey = 'uid';
}

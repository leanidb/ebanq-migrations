<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserMaZipPostalCode extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_ma_zip_postal_code';
}

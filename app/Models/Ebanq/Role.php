<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection = 'mysql';

    protected $table = 'role';
}

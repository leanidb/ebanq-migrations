<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class SecurityIncorrect extends Model
{
    protected $connection = 'mysql';

    protected $table = 'security_questions_incorrect';

    protected $primaryKey = 'aid';
}

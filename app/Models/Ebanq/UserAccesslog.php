<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserAccesslog extends Model
{
    protected $connection = 'mysql';

    protected $table = 'users_accesslog';

    protected $primaryKey = 'alid';
}

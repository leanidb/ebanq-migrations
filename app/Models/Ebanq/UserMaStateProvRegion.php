<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserMaStateProvRegion extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_ma_state_prov_region';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    protected $connection = 'mysql';

    protected $table = 'file_managed';

    protected $primaryKey = 'fid';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class BlockedIp extends Model
{
    protected $connection = 'mysql';

    protected $table = 'blocked_ips';

    protected $primaryKey = 'iid';
}

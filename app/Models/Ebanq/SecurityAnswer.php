<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class SecurityAnswer extends Model
{
    protected $connection = 'mysql';

    protected $table = 'security_questions_answers';

    protected $primaryKey = 'uid';
}

<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class UserPaAddress2ndLine extends Model
{
    protected $connection = 'mysql';

    protected $table = 'field_data_field_user_pa_address_2nd_line';
}

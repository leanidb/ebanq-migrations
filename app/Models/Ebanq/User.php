<?php

namespace App\Models\Ebanq;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'mysql';

    protected $table = 'users';

    protected $primaryKey = 'uid';

    public function firstName()
    {
        return $this->belongsTo(UserFirstName::class, 'uid', 'entity_id');
    }

    public function lastName()
    {
        return $this->belongsTo(UserLastName::class, 'uid', 'entity_id');
    }

    public function mobilePhone()
    {
        return $this->belongsTo(UserMobilePhone::class, 'uid', 'entity_id');
    }

    public function companyName()
    {
        return $this->belongsTo(UserCompanyName::class, 'uid', 'entity_id');
    }

    public function profileType()
    {
        return $this->belongsTo(ProfileType::class, 'uid', 'entity_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles', 'uid', 'rid', 'uid', 'rid');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'uid', 'entity_id');
    }

    public function documentType()
    {
        return $this->belongsTo(UserDocumentType::class, 'uid', 'entity_id');
    }

    public function dateOfBirth()
    {
        return $this->belongsTo(UserDateOfBirth::class, 'uid', 'entity_id');
    }

    public function countryOfResidence()
    {
        return $this->belongsTo(UserCountryOfResidence::class, 'uid', 'entity_id');
    }

    public function countryOfCitizenship()
    {
        return $this->belongsTo(UserCountryOfCitizenship::class, 'uid', 'entity_id');
    }

    public function group()
    {
        return $this->belongsTo(UserGroup::class, 'uid', 'entity_id');
    }

    public function userClass()
    {
        return $this->belongsTo(UserClass::class, 'uid', 'entity_id');
    }

    public function internalNotes()
    {
        return $this->belongsTo(UserInternalNotes::class, 'uid', 'entity_id');
    }

    public function paZipPostalCode()
    {
        return $this->belongsTo(UserPaZipPostalCode::class, 'uid', 'entity_id');
    }

    public function paStateProvRegion()
    {
        return $this->belongsTo(UserPaStateProvRegion::class, 'uid', 'entity_id');
    }

    public function paCountry()
    {
        return $this->belongsTo(UserPaCountry::class, 'uid', 'entity_id');
    }

    public function paCity()
    {
        return $this->belongsTo(UserPaCity::class, 'uid', 'entity_id');
    }

    public function paAddress()
    {
        return $this->belongsTo(UserPaAddress::class, 'uid', 'entity_id');
    }

    public function paAddress2ndLine()
    {
        return $this->belongsTo(UserPaAddress2ndLine::class, 'uid', 'entity_id');
    }

    public function maZipPostalCode()
    {
        return $this->belongsTo(UserMaZipPostalCode::class, 'uid', 'entity_id');
    }

    public function maStateProvRegion()
    {
        return $this->belongsTo(UserMaStateProvRegion::class, 'uid', 'entity_id');
    }

    public function maPhoneNumber()
    {
        return $this->belongsTo(UserMaPhoneNumber::class, 'uid', 'entity_id');
    }

    public function maName()
    {
        return $this->belongsTo(UserMaName::class, 'uid', 'entity_id');
    }

    public function maCountry()
    {
        return $this->belongsTo(UserMaCountry::class, 'uid', 'entity_id');
    }

    public function maCity()
    {
        return $this->belongsTo(UserMaCity::class, 'uid', 'entity_id');
    }

    public function maAsPhysical()
    {
        return $this->belongsTo(UserMaAsPhysical::class, 'uid', 'entity_id');
    }

    public function maAddress()
    {
        return $this->belongsTo(UserMaAddress::class, 'uid', 'entity_id');
    }

    public function maAddress2ndLine()
    {
        return $this->belongsTo(UserMaAddress2ndLine::class, 'uid', 'entity_id');
    }

    public function boFullName()
    {
        return $this->belongsTo(UserBoFullName::class, 'uid', 'entity_id');
    }

    public function boRelationship()
    {
        return $this->belongsTo(UserBoRelationship::class, 'uid', 'entity_id');
    }

    public function boPhoneNumber()
    {
        return $this->belongsTo(UserBoPhoneNumber::class, 'uid', 'entity_id');
    }

    public function boDateOfBirth()
    {
        return $this->belongsTo(UserBoDateOfBirth::class, 'uid', 'entity_id');
    }

    public function boPassportNumber()
    {
        return $this->belongsTo(UserBoPassportNumber::class, 'uid', 'entity_id');
    }

    public function boDocumentType()
    {
        return $this->belongsTo(UserBoDocumentType::class, 'uid', 'entity_id');
    }
}

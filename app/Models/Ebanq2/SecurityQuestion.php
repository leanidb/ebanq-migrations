<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'security_questions';

    protected $primaryKey = 'sqid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'question',
    ];
}

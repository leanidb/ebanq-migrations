<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class BlockedIp extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'blocked_ips';

    protected $primaryKey = 'iid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'ip',
    ];
}

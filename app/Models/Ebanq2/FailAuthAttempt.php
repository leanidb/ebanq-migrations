<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class FailAuthAttempt extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'fail_auth_attempts';

    protected $primaryKey = 'id';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'ip',
        'username',
        'created_at',
    ];
}

<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'users_files';

    protected $primaryKey = 'fid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'filename',
        'uri',
        'filemime',
        'filesize',
        'created_at',
    ];
}

<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class UserAccesslog extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'users_accesslog';

    protected $primaryKey = 'alid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'ip',
        'created_at',
    ];
}

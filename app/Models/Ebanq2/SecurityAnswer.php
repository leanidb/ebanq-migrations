<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class SecurityAnswer extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'security_questions_answers';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'sqid',
        'answer',
    ];
}

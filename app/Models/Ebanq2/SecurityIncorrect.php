<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class SecurityIncorrect extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'security_questions_incorrect';

    protected $primaryKey = 'aid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'sqid',
        'uid',
        'question',
        'ip',
        'created_at',
    ];
}

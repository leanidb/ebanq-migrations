<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'users';

    protected $primaryKey = 'uid';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'email',
        'username',
        'password',
        'first_name',
        'last_name',
        'phone_number',
        'company_name',
        'is_corporate',
        'role_name',
        'theme',
        'signature',
        'is_active',
        'accessed_at',
        'last_login_at',
        'updated_at',
        'created_at',
        'timezone',
        'language',
        'logo',
        'profile_type',
        'document_personal_id',
        'document_type',
        'date_of_birth',
        'country_of_residence_iso2',
        'country_of_citizenshi_iso2',
        'group',
        'class',
        'internal_notes',
        'pa_zip_postal_code',
        'pa_state_prov_region',
        'pa_country_iso',
        'pa_city',
        'pa_address',
        'pa_address_2nd_line',
        'ma_zip_postal_code',
        'ma_state_prov_region',
        'ma_phone_number',
        'ma_name',
        'ma_country',
        'ma_city',
        'ma_as_physical',
        'ma_address',
        'ma_address_2nd_line',
        'bo_full_name',
        'bo_relationship',
        'bo_phone_number',
        'bo_date_of_birth',
        'bo_document_personal_id',
        'bo_document_type',
    ];
}

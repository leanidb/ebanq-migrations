<?php

namespace App\Models\Ebanq2;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $connection = 'mysql2';

    protected $table = 'user_settings';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'internal_notification_when_executed',
        'internal_notification_when_received_transfer',
        'email_notification_when_internal_message',
        'email_notification_when_login_fails',
        'email_notification_when_funds_added',
        'email_notification_when_new_file_uploaded',
        'email_notification_when_registration_request_created',
        'email_notification_when_transfer_request_created',
        'internal_notification_when_processed',
        'internal_notification_when_processed_was_executed',
        'internal_notification_when_back_to_pending',
        'internal_notification_when_cancel_pending',
        'internal_notification_when_cancel_processed',
        'email_notification_unread_news_available',
        'email_notification_when_easytransac_transaction_fail',
    ];
}

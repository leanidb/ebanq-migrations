<?php

namespace App\Services;

use App\Models\Ebanq\User as UserOld;
//use App\Models\Ebanq2\User as UserNew;
use App\Models\Ebanq\UserSetting as UserSettingOld;
//use App\Models\Ebanq2\UserSetting as UserSettingNew;
use App\Models\Ebanq\UserFile as UserFileOld;
//use App\Models\Ebanq2\UserFile as UserFileNew;
use App\Models\Ebanq\SecurityQuestion as SecurityQuestionOld;
//use App\Models\Ebanq2\SecurityQuestion as SecurityQuestionNew;
use App\Models\Ebanq\SecurityAnswer as SecurityAnswerOld;
//use App\Models\Ebanq2\SecurityAnswer as SecurityAnswerNew;
use App\Models\Ebanq\SecurityIncorrect as SecurityIncorrectOld;
//use App\Models\Ebanq2\SecurityIncorrect as SecurityIncorrectNew;
use App\Models\Ebanq\UserAccesslog as UserAccesslogOld;
//use App\Models\Ebanq2\UserAccesslog as UserAccesslogNew;
use App\Models\Ebanq\FailAuthAttempt as FailAuthAttemptOld;
//use App\Models\Ebanq2\FailAuthAttempt as FailAuthAttemptNew;
use App\Models\Ebanq\BlockedIp as BlockedIpOld;
//use App\Models\Ebanq2\BlockedIp as BlockedIpNew;

class UserService
{
    protected $jfc;
    protected $numberOfRecords;

    public function __construct(JsonFileService $jfc)
    {
        $this->jfc = $jfc;
        $this->numberOfRecords = config('app.number_of_records');
    }

    public function migrateUsers()
    {
        UserOld::chunk($this->numberOfRecords, function ($usersOld) {
            foreach ($usersOld as $user) {
                $users[] = [
                    'uid' => $user->uid,
                    'email' => $user->mail,
                    'username' => $user->name,
                    'password' => $user->pass,
                    'first_name' => $user->firstName->field_user_first_name_value,
                    'last_name' => $user->lastName->field_user_last_name_value,
                    'phone_number' => isset($user->mobilePhone) ? $user->mobilePhone->field_user_mobile_phone_value : null,
                    'company_name' => isset($user->companyName) ? $user->companyName->field_user_company_name_value : null,
                    'is_corporate' => $user->profileType->field_profile_type_value,
                    'role_name' => $user->roles->first()->name,
                    'theme' => $user->theme,
                    'signature' => $user->signature,
                    'is_active' => $user->status,
                    'accessed_at' => !empty($user->access) ? date('Y-m-d H:i:s', $user->access) : null,
                    'last_login_at' => !empty($user->login) ? date('Y-m-d H:i:s', $user->login) : null,
                    //updated_at
                    'created_at' => date('Y-m-d H:i:s', $user->created),
                    'timezone' => $user->timezone,
                    'language' => $user->language,
                    'logo' => $user->picture,
                    'profile_type' => $user->profileType->field_profile_type_value,
                    'document_personal_id' => isset($user->document) ? $user->document->field_documents_fid : null,
                    'document_type' => isset($user->documentType)
                        ? $user->documentType->field_user_document_type_value
                        : null,
                    'date_of_birth' => isset($user->dateOfBirth)
                        ? $user->dateOfBirth->field_user_date_of_birth_value
                        : null,
                    'country_of_residence_iso2' => isset($user->countryOfResidence)
                        ? $user->countryOfResidence->field_user_country_of_residence_iso2
                        : null,
                    'country_of_citizenshi_iso2' => isset($user->countryOfCitizenship)
                        ? $user->countryOfCitizenship->field_user_country_of_citizenshi_iso2
                        : null,
                    'group' => isset($user->group) ? $user->group->field_user_group_nid : null,
                    'class' => isset($user->userClass) ? $user->userClass->field_user_class_nid : null,
                    'internal_notes' => isset($user->internalNotes)
                        ? $user->internalNotes->field_user_internal_notes_value
                        : null,
                    'pa_zip_postal_code' => isset($user->paZipPostalCode)
                        ? $user->paZipPostalCode->field_user_pa_zip_postal_code_value
                        : null,
                    'pa_state_prov_region' => isset($user->paStateProvRegion)
                        ? $user->paStateProvRegion->field_user_pa_state_prov_region_value
                        : null,
                    'pa_country_iso' => isset($user->paCountry) ? $user->paCountry->field_user_pa_country_iso2 : null,
                    'pa_city' => isset($user->paCity) ? $user->paCity->field_user_pa_city_value : null,
                    'pa_address' => isset($user->paAddress) ? $user->paAddress->field_user_pa_address_value : null,
                    'pa_address_2nd_line' => isset($user->paAddress2ndLine)
                        ? $user->paAddress2ndLine->field_user_pa_address_2nd_line_value
                        : null,
                    'ma_zip_postal_code' => isset($user->maZipPostalCode)
                        ? $user->maZipPostalCode->field_user_ma_zip_postal_code_value
                        : null,
                    'ma_state_prov_region' => isset($user->maStateProvRegion)
                        ? $user->maStateProvRegion->field_user_ma_state_prov_region_value
                        : null,
                    'ma_phone_number' => isset($user->maPhoneNumber)
                        ? $user->maPhoneNumber->field_user_ma_phone_number_value
                        : null,
                    'ma_name' => isset($user->maName) ? $user->maName->field_user_ma_name_value : null,
                    'ma_country' => isset($user->maCountry) ? $user->maCountry->field_user_ma_country_iso2 : null,
                    'ma_city' => isset($user->maCity) ? $user->maCity->field_user_ma_city_value : null,
                    'ma_as_physical' => isset($user->maAsPhysical)
                        ? $user->maAsPhysical->field_user_ma_as_physical_value
                        : null,
                    'ma_address' => isset($user->maAddress) ? $user->maAddress->field_user_ma_address_value : null,
                    'ma_address_2nd_line' => isset($user->maAddress2ndLine)
                        ? $user->maAddress2ndLine->field_user_ma_address_2nd_line__value
                        : null,
                    'bo_full_name' => isset($user->boFullName) ? $user->boFullName->field_user_bo_full_name_value : null,
                    'bo_relationship' => isset($user->boRelationship)
                        ? $user->boRelationship->field_user_bo_relationship_value
                        : null,
                    'bo_phone_number' => isset($user->boPhoneNumber)
                        ? $user->boPhoneNumber->field_user_bo_phone_number_value
                        : null,
                    'bo_date_of_birth' => isset($user->boDateOfBirth)
                        ? $user->boDateOfBirth->field_user_bo_date_of_birth_value
                        : null,
                    'bo_document_personal_id' => isset($user->boPassportNumber)
                        ? $user->boPassportNumber->field_user_bo_passport_number_value
                        : null,
                    'bo_document_type' => isset($user->boDocumentType)
                        ? $user->boDocumentType->field__user_bo_document_type_value
                        : null,
                ];
            }

            if (isset($users)) {
                //UserNew::insert($users);
                $this->jfc->fill('users.json', $users);
            }
        });
    }

    public function migrateUserSettings()
    {
        UserSettingOld::chunk($this->numberOfRecords, function ($settingsOld) {
            foreach ($settingsOld as $setting) {
                $usersSettings[] = [
                    'uid' => $setting->uid,
                    'internal_notification_when_executed' => $setting->internal_notification_when_executed,
                    'internal_notification_when_received_transfer' =>
                        $setting->internal_notification_when_received_transfer,
                    'email_notification_when_internal_message' => $setting->email_notification_when_internal_message,
                    'email_notification_when_login_fails' => $setting->email_notification_when_login_fails,
                    'email_notification_when_funds_added' => $setting->email_notification_when_funds_added,
                    'email_notification_when_new_file_uploaded' => $setting->email_notification_when_new_file_uploaded,
                    'email_notification_when_registration_request_created' =>
                        $setting->email_notification_when_registration_request_created,
                    'email_notification_when_transfer_request_created' =>
                        $setting->email_notification_when_transfer_request_created,
                    'internal_notification_when_processed' => $setting->internal_notification_when_processed,
                    'internal_notification_when_processed_was_executed' =>
                        $setting->internal_notification_when_processed_was_executed,
                    'internal_notification_when_back_to_pending' => $setting->internal_notification_when_back_to_pending,
                    'internal_notification_when_cancel_pending' => $setting->internal_notification_when_cancel_pending,
                    'internal_notification_when_cancel_processed' => $setting->internal_notification_when_cancel_processed,
                    'email_notification_unread_news_available' => $setting->email_notification_unread_news_available,
                    'email_notification_when_easytransac_transaction_fail' =>
                        $setting->email_notification_when_easytransac_transaction_fail,
                ];
            }

            if (isset($usersSettings)) {
                //UserSettingNew::insert($usersSettings);
                $this->jfc->fill('user_settings.json', $usersSettings);
            }
        });
    }

    public function migrateUserFiles()
    {
        UserFileOld::chunk($this->numberOfRecords, function ($filesOld) {
            foreach ($filesOld as $file) {
                $usersFiles[] = [
                    'fid' => $file->fid,
                    'uid' => $file->uid,
                    'filename' => $file->filename,
                    'uri' => $file->uri,
                    'filemime' => $file->filemime,
                    'filesize' => $file->filesize,
                    'created_at' => date('Y-m-d H:i:s', $file->timestamp),
                ];
            }

            if (isset($usersFiles)) {
                //UserFileNew::insert($usersFiles);
                $this->jfc->fill('users_files.json', $usersFiles);
            }
        });
    }

    public function migrateSecurityQuestions()
    {
        SecurityQuestionOld::chunk($this->numberOfRecords, function ($securityQuestionsOld) {
            foreach ($securityQuestionsOld as $securityQuestion) {
                $securityQuestions[] = [
                    'sqid' => $securityQuestion->sqid,
                    //type
                    'uid' => $securityQuestion->uid,
                    'question' => $securityQuestion->question,
                ];
            }

            if (isset($securityQuestions)) {
                //SecurityQuestionNew::insert($securityQuestions);
                $this->jfc->fill('security_questions.json', $securityQuestions);
            }
        });
    }

    public function migrateSecurityAnswers()
    {
        SecurityAnswerOld::chunk($this->numberOfRecords, function ($securityAnswersOld) {
            foreach ($securityAnswersOld as $securityAnswer) {
                $securityAnswers[] = [
                    'uid' => $securityAnswer->uid,
                    'sqid' => $securityAnswer->sqid,
                    'answer' => $securityAnswer->answer,
                ];
            }

            if (isset($securityAnswers)) {
                //SecurityAnswerNew::insert($securityAnswers);
                $this->jfc->fill('security_questions_answers.json', $securityAnswers);
            }
        });
    }

    public function migrateSecurityIncorrects()
    {
        SecurityIncorrectOld::chunk($this->numberOfRecords, function ($securityIncorrectsOld) {
            foreach ($securityIncorrectsOld as $securityIncorrect) {
                $securityIncorrects[] = [
                    'aid' => $securityIncorrect->aid,
                    'sqid' => $securityIncorrect->sqid,
                    'uid' => $securityIncorrect->uid,
                    'ip' => $securityIncorrect->ip,
                    'created_at' => date('Y-m-d H:i:s', $securityIncorrect->timestamp),
                ];
            }

            if (isset($securityIncorrects)) {
                //SecurityIncorrectNew::insert($securityIncorrects);
                $this->jfc->fill('security_questions_incorrect.json', $securityIncorrects);
            }
        });
    }

    public function migrateUserAccesslogs()
    {
        UserAccesslogOld::chunk($this->numberOfRecords, function ($accesslogsOld) {
            foreach ($accesslogsOld as $accesslog) {
                $usersAccesslogs[] = [
                    'alid' => $accesslog->alid,
                    'uid' => $accesslog->uid,
                    'ip' => $accesslog->ip,
                    'created_at' => date('Y-m-d H:i:s', $accesslog->login),
                ];
            }

            if (isset($usersAccesslogs)) {
                //UserAccesslogNew::insert($usersAccesslogs);
                $this->jfc->fill('users_accesslog.json', $usersAccesslogs);
            }
        });
    }

    public function migrateFailAuthAttempts()
    {
        FailAuthAttemptOld::chunk($this->numberOfRecords, function ($failAuthAttemptsOld) {
            foreach ($failAuthAttemptsOld as $failAuthAttempt) {
                $failAuthAttempts[] = [
                    'id' => $failAuthAttempt->id,
                    'ip' => $failAuthAttempt->ip,
                    'username' => $failAuthAttempt->username,
                    'created_at' => date('Y-m-d H:i:s', $failAuthAttempt->stamp),
                ];
            }

            if (isset($failAuthAttempts)) {
                //FailAuthAttemptNew::insert($failAuthAttempts);
                $this->jfc->fill('fail_auth_attempts.json', $failAuthAttempts);
            }
        });
    }

    public function migrateBlockedIps()
    {
        BlockedIpOld::chunk($this->numberOfRecords, function ($blockedIpsOld) {
            foreach ($blockedIpsOld as $blockedIp) {
                $blockedIps[] = [
                    'iid' => $blockedIp->iid,
                    'ip' => $blockedIp->ip,
                ];
            }

            if (isset($blockedIps)) {
                //BlockedIpNew::insert($blockedIps);
                $this->jfc->fill('blocked_ips.json', $blockedIps);
            }
        });
    }
}

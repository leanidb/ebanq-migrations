<?php

namespace App\Services;

class JsonFileService
{
    public function fill($filename, $data)
    {
        $filename = config('app.path_to_json_files') . $filename;
        $handle = (file_exists($filename)) ? fopen($filename, 'r+') : fopen($filename, 'w+');

        fseek($handle, 0, SEEK_END);
        if (ftell($handle)) {
            fseek($handle, -1, SEEK_END);
            fwrite($handle, ',' . trim(json_encode($data), '[]') . ']');
        } else {
            fwrite($handle, '[' . trim(json_encode($data), '[]') . ']');
        }
        fclose($handle);
    }
}

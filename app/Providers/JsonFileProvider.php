<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\JsonFileService;

class JsonFileProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(JsonFileService::class, function ($app) {

            return new JsonFileService();
        });
    }
}
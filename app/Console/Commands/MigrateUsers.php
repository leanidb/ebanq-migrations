<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\JsonFileService;
use App\Services\UserService;

class MigrateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migration tables of users completed successfully.\n";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param JsonFileService $jfc
     *
     * @return mixed
     */
    public function handle(JsonFileService $jfc)
    {
        try {
            $user = new UserService($jfc);
            $user->migrateUsers();
            $user->migrateUserSettings();
            $user->migrateUserFiles();
            $user->migrateUserAccesslogs();
            $user->migrateSecurityQuestions();
            $user->migrateSecurityAnswers();
            $user->migrateSecurityIncorrects();
            $user->migrateFailAuthAttempts();
            $user->migrateBlockedIps();
            echo $this->description;
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FullMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migration of all tables completed successfully.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //$this->call('migrate');
            $this->call('migrate:users');
            echo $this->description;
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}

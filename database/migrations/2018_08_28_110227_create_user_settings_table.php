<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->increments('sid');
            $table->string('uid');
            $table->tinyInteger('internal_notification_when_executed')->nullable();
            $table->tinyInteger('internal_notification_when_received_transfer')->nullable();
            $table->tinyInteger('email_notification_when_internal_message')->nullable();
            $table->tinyInteger('email_notification_when_login_fails')->nullable();
            $table->tinyInteger('email_notification_when_funds_added')->nullable();
            $table->tinyInteger('email_notification_when_new_file_uploaded')->nullable();
            $table->tinyInteger('email_notification_when_registration_request_created')->nullable();
            $table->tinyInteger('email_notification_when_transfer_request_created')->nullable();
            $table->tinyInteger('internal_notification_when_processed');
            $table->tinyInteger('internal_notification_when_processed_was_executed');
            $table->tinyInteger('internal_notification_when_back_to_pending');
            $table->tinyInteger('internal_notification_when_cancel_pending');
            $table->tinyInteger('internal_notification_when_cancel_processed');
            $table->tinyInteger('email_notification_unread_news_available')->nullable();
            $table->tinyInteger('email_notification_when_easytransac_transaction_fail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->create('users', function (Blueprint $table) {
            $table->string('uid');
            $table->string('email');
            $table->string('username');
            $table->string('password');
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->string('phone_number', 45)->nullable();
            $table->string('company_name', 45)->nullable();
            $table->tinyInteger('is_corporate');
            $table->string('role_name', 45);
            $table->string('theme')->nullable();
            $table->string('signature')->nullable();
            $table->tinyInteger('is_active');
            $table->dateTime('accessed_at')->nullable();
            $table->dateTime('last_login_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('created_at');
            $table->string('timezone', 32)->nullable();
            $table->string('language', 12)->nullable();
            $table->integer('logo')->length(11)->nullable();
            $table->tinyInteger('profile_type');
            $table->string('document_personal_id')->nullable();
            $table->tinyInteger('document_type')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('country_of_residence_iso2', 2)->nullable();
            $table->string('country_of_citizenshi_iso2', 2)->nullable();
            $table->integer('group')->length(11)->nullable();
            $table->integer('class')->length(11)->nullable();
            $table->longText('internal_notes')->nullable();
            $table->string('pa_zip_postal_code', 45)->nullable();
            $table->string('pa_state_prov_region')->nullable();
            $table->string('pa_country_iso', 2)->nullable();
            $table->string('pa_city', 45)->nullable();
            $table->string('pa_address')->nullable();
            $table->string('pa_address_2nd_line')->nullable();
            $table->string('ma_zip_postal_code', 45)->nullable();
            $table->string('ma_state_prov_region')->nullable();
            $table->string('ma_phone_number', 45)->nullable();
            $table->string('ma_name')->nullable();
            $table->string('ma_country', 45)->nullable();
            $table->string('ma_city')->nullable();
            $table->tinyInteger('ma_as_physical')->nullable();
            $table->string('ma_address')->nullable();
            $table->string('ma_address_2nd_line')->nullable();
            $table->string('bo_full_name', 45)->nullable();
            $table->string('bo_relationship', 45)->nullable();
            $table->string('bo_phone_number', 45)->nullable();
            $table->date('bo_date_of_birth')->nullable();
            $table->string('bo_document_personal_id')->nullable();
            $table->tinyInteger('bo_document_type')->nullable();

            $table->primary(['uid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->dropIfExists('users');
    }
}
